# startme

> A test assignment for start.me

## Build Setup

``` bash
# install yarn and serve
npm install -g yarn serve

# install dependencies
yarn

# build for production with minification
npm run build

# start application (it will run on http://localhost:5000)
npm run build
serve dist/

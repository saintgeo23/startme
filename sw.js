/* eslint-disable no-console */
console.log('sw!');
const CACHE_NAME = 'startme-cache';
const file = '/static/pageData.json';
const origin = self.location.origin;
let timestamp = null;
let needSync = true;

function putToCache(data) {
  if (!needSync) {
    return false;
  }

  timestamp = data.timestamp;
  console.log('[install] Adding files from JSON file: ', file);
  const blob = new Blob([JSON.stringify(data, null, 2)], { type: 'application/json' });
  const res = new Response(blob);

  caches.open(CACHE_NAME)
    .then(cache => cache.put(`${origin}${file}`, res))
}

function getResource() {
  fetch(`.${file}`)
    .then(response => response.json())
    .then((data) => {
      needSync = true;

      if (timestamp === data.timestamp) {
        needSync = false;
      }

      return putToCache(data);
    })
    .then(() => {
      if (!needSync) {
        return false;
      }

      return self.clients.matchAll({
        includeUncontrolled: true,
      })
        .then((clients) => {
          clients.forEach((client) => {
            const message = {
              type: 'refresh',
            };

            client.postMessage(JSON.stringify(message));
          });
        });
    });
}

self.addEventListener('install', (event) => {
  console.log('[install] Kicking off service worker registration!');

  event.waitUntil(
    caches.open(CACHE_NAME)
      .then((cache) => {
        return fetch(`.${file}`)
          .then(response => response.json())
          .then(putToCache)
          .catch((err) => {
            console.log('err', err);
          });
      })
      .then(() => {
        console.log(
          '[install] All required resources have been cached;',
          'the Service Worker was successfully installed!',
        );

        return self.skipWaiting();
      }),
  );
});

self.addEventListener('fetch', (event) => {
  event.respondWith(
    caches.match(event.request)
      .then((response) => {
        if (response) {
          console.log(
            '[fetch] Returning from Service Worker cache: ',
            event.request.url,
          );
          return response;
        }

        console.log('[fetch] Returning from server: ', event.request.url);
        return fetch(event.request);
      },
    ),
  );
});

self.addEventListener('sync', (event) => {
  if (event.tag === 'get-new-data') {
    event.waitUntil(
      getResource()
    );
  }
});

self.addEventListener('activate', (event) => {
  console.log('[activate] Activating service worker!');
  console.log('[activate] Claiming this service worker!');
  event.waitUntil(self.clients.claim());
});

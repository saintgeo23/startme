import Page from '@/components/Page';

export default [
  {
    path: '/:id',
    name: 'MainPage',
    component: Page,
  },
];

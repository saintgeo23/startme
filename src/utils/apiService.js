import axios from 'axios';

export default {

  getData(id) {
    return axios.get(`https://startme-server.herokuapp.com/${id}`, { timeout: 4000 });
  },
};

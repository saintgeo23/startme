import ApiService from '@/utils/apiService';
import createPersist from 'vuex-localstorage';

export default {
  state: {
    pageData: {},
  },

  actions: {
    getData({ commit }, id) {
      return new Promise((resolve, reject) => {
        ApiService.getData(id)
          .then((data) => {
            commit('setPageData', { data: data.data, id });
            resolve(data.data);
          })
          .catch(reject);
      });
    },
  },
  mutations: {
    /* eslint-disable no-param-reassign */
    setPageData(state, { data, id }) {
      state.pageData[id] = data;
    },
  },

  getters: {
  },

  plugins: [createPersist({
    namespace: 'startme',
    initialState: {},
    expires: 7 * 24 * 60 * 60 * 1e3,
  })],
};

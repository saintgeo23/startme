// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import App from './App';

import routes from './router';
import storage from './store';

Vue.use(Vuex);
const store = new Vuex.Store(storage);

Vue.use(VueRouter);
const router = new VueRouter({
  routes,
  scrollBehavior: () => ({ y: 0 }),
});

Vue.config.productionTip = false;

function checkID(to, from, next) {
  if (to.path === '/') {
    return next('/3gykOZ');
  }

  return next();
}

router.beforeEach(checkID);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  store,
  router,
});
